package ass.netcom.listener;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.io.Tcp;
import akka.io.Tcp.Bound;
import akka.io.Tcp.Connected;
import akka.io.Tcp.ConnectionClosed;
import akka.io.Tcp.Received;
import akka.io.TcpMessage;
import ass.addressconverter.AddressConverter;
import ass.log.Logger;
import ass.messages.external.ExternalMessage;
import ass.messages.external.players.*;
import ass.messages.external.puzzle.*;
import ass.messages.internal.netcom.ListenerBoundMessage;
import ass.model.cs.LogicalClock;

public class ListenerActor extends AbstractActor {
	
	private final Logger log;

	private final ActorRef controller;

	private final LogicalClock logicalClock;
	
	public ListenerActor(final ActorRef controller, final LogicalClock logicalClock, final String hostname, final int port) {
		this.controller = controller;
		this.logicalClock = logicalClock;
		this.log = new Logger("ACTOR " + port, this.getClass());
		this.bindTcpManager(hostname, port);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(Bound.class, msg -> {
					log.info("Listener bound");
					controller.tell(new ListenerBoundMessage(msg.localAddress()), getSelf());
				})
				.match(Connected.class, msg -> {
					getSender().tell(TcpMessage.register(getSelf()), getSelf());
					log.info("Connected to: " + msg.remoteAddress().toString());
				})
				.match(Received.class, msg -> {
					this.handleReceivedMessage(msg.data().utf8String());
				})
				.match(ConnectionClosed.class, msg -> {
					log.info("Connection closed: " + msg);
				})
				.build();
	}

	private void bindTcpManager(final String hostname, final int port) {
		final ActorRef tcpManager = Tcp.get(getContext().getSystem()).manager();
		tcpManager.tell(TcpMessage.bind(getSelf(), new InetSocketAddress(hostname, port), 100), getSelf());
	}

	private void handleReceivedMessage(final String dataString) {
		final String[] splitData = dataString.split("/");
		ExternalMessage receivedMessage = null;
		switch (splitData[0]) {
			case PlayerJoinedMessage.TYPE_STRING:
				final Optional<InetSocketAddress> joinedAddress = AddressConverter.addressFromString(splitData[1]);
				if (joinedAddress.isPresent()) { receivedMessage = new PlayerJoinedMessage(joinedAddress.get()); }
				break;
			case PlayerLeftMessage.TYPE_STRING:
				final Optional<InetSocketAddress> leftAddress = AddressConverter.addressFromString(splitData[1]);
				if (leftAddress.isPresent()) { receivedMessage = new PlayerLeftMessage(leftAddress.get()); }
				break;
			case PlayerJoinRequestMessage.TYPE_STRING:
				final Optional<InetSocketAddress> joinReqAddress = AddressConverter.addressFromString(splitData[1]);
				if (joinReqAddress.isPresent()) { receivedMessage = new PlayerJoinRequestMessage(joinReqAddress.get()); }
				break;
			case PlayerJoinResponseMessage.TYPE_STRING:
				receivedMessage = new PlayerJoinResponseMessage(Boolean.parseBoolean(splitData[1]));
				break;
			case PlayerListMessage.TYPE_STRING:
				final List<InetSocketAddress> playerList = new ArrayList<>();
				Arrays.stream(splitData).skip(1).forEach(s -> AddressConverter.addressFromString(s).ifPresent(playerList::add));
				receivedMessage = new PlayerListMessage(playerList);
				break;
			case TileLockRequestMessage.TYPE_STRING:
				final Optional<InetSocketAddress> tileLockReqAddress = AddressConverter.addressFromString(splitData[1]);
				if (tileLockReqAddress.isPresent()) {
					receivedMessage = new TileLockRequestMessage(tileLockReqAddress.get(), logicalClock.getTimestamp(), Integer.parseInt(splitData[2]), Integer.parseInt(splitData[3]));
				}
				break;
			case TileLockReplyMessage.TYPE_STRING:
				final Optional<InetSocketAddress> tileLockRepAddress = AddressConverter.addressFromString(splitData[2]);
				if (tileLockRepAddress.isPresent()) {
					int timestampReceived = Integer.parseInt(splitData[1]);
					logicalClock.updateClock(timestampReceived);
					receivedMessage = new TileLockReplyMessage(tileLockRepAddress.get(), timestampReceived, Integer.parseInt(splitData[3]), Integer.parseInt(splitData[4]));
				}
				break;
			case TileSwapMessage.TYPE_STRING:
				receivedMessage = new TileSwapMessage(Integer.parseInt(splitData[1]), Integer.parseInt(splitData[2]), Integer.parseInt(splitData[3]), Integer.parseInt(splitData[4]));
				break;
			case PuzzleCompletedMessage.TYPE_STRING:
				receivedMessage = new PuzzleCompletedMessage();
				break;
			case PuzzleTilesStateMessage.TYPE_STRING:
				receivedMessage = new PuzzleTilesStateMessage(splitData[1]);
				break;
			case TileLockConfirmedMessage.TYPE_STRING:
				receivedMessage = new TileLockConfirmedMessage(Integer.parseInt(splitData[1]), Integer.parseInt(splitData[2]));
				break;
			default:
				log.error("Message not recognized");
				break;
		}
		if (receivedMessage != null) {
			log.debug("Received a message: " + receivedMessage.toSendFormat());
			controller.tell(receivedMessage, getSelf());
		}
	}
}
