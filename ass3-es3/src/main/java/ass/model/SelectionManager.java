package ass.model;

import ass.view.data.Tile;

/**
 * This class is hooked to each tile to react to selection events.
 */
public class SelectionManager {

    private boolean selectionActive = false;
    private Tile selectedTile;

    public void selectTile(final Tile tile, final Listener listener) {
        if (selectionActive) {
            selectionActive = false;

            swap(selectedTile, tile);

            listener.onSwapPerformed(selectedTile);
        } else {
            selectionActive = true;
            selectedTile = tile;
        }
    }

    private void swap(final Tile t1, final Tile t2) {
        int pos = t1.getCurrentPosition();
        t1.setCurrentPosition(t2.getCurrentPosition());
        t2.setCurrentPosition(pos);
    }

    public boolean isSelectionActive() {
        return selectionActive;
    }

    @FunctionalInterface
    public interface Listener {
        void onSwapPerformed(Tile selectedTile);
    }
}
