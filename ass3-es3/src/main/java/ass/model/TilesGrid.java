package ass.model;

import ass.config.NodeConfig;
import ass.view.data.Tile;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class TilesGrid {

    private final int rows;
    private final int columns;
    private final String imagePath;

    private final List<Image> imageCache;
    private final List<Tile> tiles;
    private final List<Tile> selectedTiles;

    private Tile currentlySelectedTile;

    public TilesGrid(NodeConfig config) {
        this.rows = config.getRows();
        this.columns = config.getCols();
        this.imagePath = config.getImagePath();
        this.imageCache = new LinkedList<>();
        this.tiles = new LinkedList<>();
        this.selectedTiles = new LinkedList<>();
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public String getImagePath() {
        return imagePath;
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public List<Image> getImageCache() {
        return imageCache;
    }

    public List<Tile> getSelectedTiles() {
        return selectedTiles;
    }

    public Tile getCurrentlySelectedTile() {
        return currentlySelectedTile;
    }

    public void setCurrentlySelectedTile(Tile currentlySelectedTile) {
        this.currentlySelectedTile = currentlySelectedTile;
    }
}
