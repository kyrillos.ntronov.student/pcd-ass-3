package ass.model.cs;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TilesCriticalSectionsManager {

    private final List<PendingRequest> pendingRequests = new ArrayList<>();

    public void makeNewRequest(final List<InetSocketAddress> knownNodes, final int row, final int col, final PendingRequest.Listener listener) {
        pendingRequests.add(new PendingRequest(knownNodes, row, col, listener));
    }

    public void registerReply(InetSocketAddress node, int row, int col) {
        Optional<PendingRequest> currentPendingRequest = pendingRequests.stream().filter(r -> r.getRow() == row && r.getCol() == col).findFirst();
        currentPendingRequest.ifPresent(pendingRequest -> {
            pendingRequest.registerReply(node);
            if (pendingRequest.isAllRepliesReceived()) {
                this.pendingRequests.remove(pendingRequest);
            }
        });
    }

    /**
     * Returns whether there is already a request to lock a tile.
     */
    public boolean isLockAvailable(int row, int col) {
       return pendingRequests.stream().noneMatch(r -> r.getRow() == row && r.getCol() == col);
    }
}

