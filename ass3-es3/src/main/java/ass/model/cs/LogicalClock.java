package ass.model.cs;

public class LogicalClock {

    private int tick;

    public int getTimestamp() {
        tick++;
        return tick;
    }

    public void updateClock(int timestampReceived) {
        if(timestampReceived > tick) {
            this.tick = timestampReceived;
        }
    }
}
