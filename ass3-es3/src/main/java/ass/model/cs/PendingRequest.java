package ass.model.cs;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PendingRequest {

    private final Map<InetSocketAddress, Boolean> repliesReceived;
    private final PendingRequest.Listener listener;

    private final int row;
    private final int col;

    public PendingRequest(List<InetSocketAddress> knownNodes, int row, int col, Listener listener) {
        this.repliesReceived = new HashMap<>();
        for (InetSocketAddress knownNode : knownNodes) {
            repliesReceived.put(knownNode, false);
        }
        this.row = row;
        this.col = col;
        this.listener = listener;
    }

    public void registerReply(InetSocketAddress node) {
        repliesReceived.put(node, true);
        if(isAllRepliesReceived()) {
            listener.onRepliesReceived();
        }
    }

    public boolean isAllRepliesReceived() {
        return repliesReceived.isEmpty() || repliesReceived.values().stream().allMatch(Boolean::booleanValue);
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @FunctionalInterface
    public interface Listener {
        void onRepliesReceived();
    }
}
