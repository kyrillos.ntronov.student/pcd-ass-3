package ass.messages.internal.wiring;

import akka.actor.ActorRef;
import ass.messages.internal.InternalMessage;

public class AttachViewMessage implements InternalMessage {

    public final ActorRef viewActor;

    public AttachViewMessage(final ActorRef viewActor) {
        this.viewActor = viewActor;
    }
}
