package ass.messages.internal.selection;

import ass.messages.internal.InternalMessage;

public class ViewSelectTileRequestMessage implements InternalMessage {
    public final int row;
    public final int col;

    public ViewSelectTileRequestMessage(int row, int col) {
        this.row = row;
        this.col = col;
    }
}
