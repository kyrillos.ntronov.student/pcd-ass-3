package ass.messages.internal.netcom;

import ass.messages.internal.InternalMessage;

public class ConnectToMessage implements InternalMessage {

	public final String connectString;
	
	public ConnectToMessage(final String connectString) {
		this.connectString = connectString;
	}
	
}
