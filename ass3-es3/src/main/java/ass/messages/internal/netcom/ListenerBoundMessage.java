package ass.messages.internal.netcom;

import ass.messages.internal.InternalMessage;

import java.net.InetSocketAddress;

public class ListenerBoundMessage implements InternalMessage {
	public final InetSocketAddress localAddress;
	
	public ListenerBoundMessage(final InetSocketAddress localAddress) {
		this.localAddress = localAddress;
	}
}
