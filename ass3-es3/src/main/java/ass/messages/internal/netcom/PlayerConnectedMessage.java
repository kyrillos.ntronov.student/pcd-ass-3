package ass.messages.internal.netcom;

import ass.messages.internal.InternalMessage;

import java.net.InetSocketAddress;
import java.util.List;

public class PlayerConnectedMessage implements InternalMessage {

    private final String playerName;
    private final List<InetSocketAddress> knownNodes;

    public PlayerConnectedMessage(String playerName, List<InetSocketAddress> knownNodes) {
        this.playerName = playerName;
        this.knownNodes = knownNodes;
    }

    public String getPlayerName() {
        return playerName;
    }

    public List<InetSocketAddress> getKnownNodes() {
        return knownNodes;
    }
}

