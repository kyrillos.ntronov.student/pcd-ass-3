package ass.messages.external.players;

import ass.messages.external.ExternalMessage;

import java.net.InetSocketAddress;

/**
 * A message to notify the other players that the sender is disconnecting from the game.
 */
public class PlayerLeftMessage implements ExternalMessage {

	public static final String TYPE_STRING = "PLAYER_LEFT";

	private final InetSocketAddress socketAddress;

	public PlayerLeftMessage(final String hostname, final int port) {
		this.socketAddress = new InetSocketAddress(hostname, port);
	}

	public PlayerLeftMessage(final InetSocketAddress address) {
		this.socketAddress = address;
	}

	public InetSocketAddress getSocketAddress() {
		return this.socketAddress;
	}

	@Override
	public String toSendFormat() {
		return TYPE_STRING + "/" + socketAddress.getHostName() + ":" + socketAddress.getPort();
	}

}
