package ass.messages.external.players;

import ass.messages.external.ExternalMessage;

import java.net.InetSocketAddress;

/**
 * A message for request to join a game to a player by a non-player.
 */
public class PlayerJoinRequestMessage implements ExternalMessage {

	public static final String TYPE_STRING = "PLAYER_JOIN_REQUEST";

	private final InetSocketAddress socketAddress;

	public PlayerJoinRequestMessage(final String hostname, final int port) {
		this.socketAddress = new InetSocketAddress(hostname, port);
	}

	public PlayerJoinRequestMessage(final InetSocketAddress address) {
		this.socketAddress = address;
	}

	public InetSocketAddress getSocketAddress() {
		return this.socketAddress;
	}

	@Override
	public String toSendFormat() {
		return TYPE_STRING + "/" + socketAddress.getHostName() + ":" + socketAddress.getPort();
	}
}
