package ass.messages.external.puzzle;

import ass.messages.external.ExternalMessage;

/**
 * A message to notify a tile swap.
 */
public class TileSwapMessage implements ExternalMessage {

	public final static String TYPE_STRING = "TILE_SWAP";

	public final int fromRow;
	public final int fromColumn;
	public final int toRow;
	public final int toColumn;

	public TileSwapMessage(final int fromRow, final int fromColumn, final int toRow, final int toColumn) {
		this.fromRow = fromRow;
		this.fromColumn = fromColumn;
		this.toRow = toRow;
		this.toColumn = toColumn;
	}

	@Override
	public String toSendFormat() {
		return TYPE_STRING + "/" + this.fromRow + "/" + this.fromColumn + "/" + this.toRow + "/" + this.toColumn;
	}

}
