package ass.messages.external.puzzle;

import ass.addressconverter.AddressConverter;
import ass.messages.external.ExternalMessage;

import java.net.InetSocketAddress;

/**
 * A message to request the locking of a tile of the puzzle.
 */
public class TileLockRequestMessage implements ExternalMessage {

	public static final String TYPE_STRING = "TILE_LOCK_REQ";

	public final InetSocketAddress requesterAddress;
	public final int timestamp;

	public final int row;
	public final int column;

	public TileLockRequestMessage(final InetSocketAddress address, final int timestamp, final int row, final int column) {
		this.requesterAddress = address;
		this.timestamp = timestamp;
		this.row = row;
		this.column = column;
	}

	@Override
	public String toSendFormat() {
		return TYPE_STRING + "/" + AddressConverter.stringFromAddress(this.requesterAddress) + "/" + row + "/" + column;
	}

}
