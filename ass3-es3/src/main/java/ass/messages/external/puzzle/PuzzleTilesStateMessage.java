package ass.messages.external.puzzle;

import ass.messages.data.TilesList;
import ass.messages.external.ExternalMessage;
import ass.messages.json.JsonConverter;

public class PuzzleTilesStateMessage implements ExternalMessage {

    public static final String TYPE_STRING = "TILE_STATE";

    private final TilesList tiles;

    public PuzzleTilesStateMessage(TilesList tiles) {
        this.tiles = tiles;
    }

    public PuzzleTilesStateMessage(String tiles) {
        this.tiles = JsonConverter.jsonToObject(tiles, TilesList.class);
    }

    public TilesList getTiles() {
        return tiles;
    }

    @Override
    public String toSendFormat() {
        return TYPE_STRING + "/" + JsonConverter.objectToJson(tiles);
    }

}
