package ass.config;

public class NodeConfig {

    private final int rows;
    private final int cols;
    private final String imagePath;
    private final int maxPlayers;
    private final int heartbeatTimeout;
    private final int port;

    public NodeConfig(int rows, int cols, String imagePath, int maxPlayers, int heartbeatTimeout, int port) {
        this.rows = rows;
        this.cols = cols;
        this.imagePath = imagePath;
        this.maxPlayers = maxPlayers;
        this.heartbeatTimeout = heartbeatTimeout;
        this.port = port;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public String getImagePath() {
        return imagePath;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public int getHeartbeatTimeout() {
        return heartbeatTimeout;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "NodeConfig{" +
                "rows=" + rows +
                ", cols=" + cols +
                ", imagePath='" + imagePath + '\'' +
                ", maxPlayers=" + maxPlayers +
                ", heartbeatTimeout=" + heartbeatTimeout +
                '}';
    }
}
