package ass.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class NodeConfigLoader {

    private static final String PROPERTIES_FILE_NAME = "node.properties";

    private int port;

    public NodeConfigLoader(String port) {
        this.port = Integer.parseInt(port);
    }

    public NodeConfig loadProperties() throws IOException {
        Properties properties = new Properties();
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)) {
            if (inputStream != null) {
                properties.load(inputStream);
                int rows = Integer.parseInt(properties.getProperty("board.rows"));
                int cols = Integer.parseInt(properties.getProperty("board.cols"));
                String imagePath = properties.getProperty("board.image");
                int maxPlayers = Integer.parseInt(properties.getProperty("node.max.players"));
                int heartbeatTimeout = Integer.parseInt(properties.getProperty("node.heartbeat.timeout"));
                return new NodeConfig(rows, cols, imagePath, maxPlayers, heartbeatTimeout, port);
            } else {
                throw new FileNotFoundException("Cannot load specified properties: " + PROPERTIES_FILE_NAME);
            }
        }
    }

}
