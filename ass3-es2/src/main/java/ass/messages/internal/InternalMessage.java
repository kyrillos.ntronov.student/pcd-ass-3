package ass.messages.internal;

/**
 * An internal message is a message between components of a single instance, 
 * i.e. View, Controller and Network communication manager.
 */
public interface InternalMessage {

}
