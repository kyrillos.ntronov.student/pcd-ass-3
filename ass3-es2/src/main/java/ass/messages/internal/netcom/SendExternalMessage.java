package ass.messages.internal.netcom;

import java.net.InetSocketAddress;

import ass.messages.external.ExternalMessage;
import ass.messages.internal.InternalMessage;

public class SendExternalMessage implements InternalMessage {

	public final InetSocketAddress socketAddress;
	public final ExternalMessage msg;
	
	public SendExternalMessage(final InetSocketAddress socketAddress, final ExternalMessage msg) {
		this.socketAddress = socketAddress;
		this.msg = msg;
	}
	
}
