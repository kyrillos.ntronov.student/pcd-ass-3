package ass.messages.internal.netcom;

import ass.messages.internal.InternalMessage;

import java.net.InetSocketAddress;

public class PlayerDisconnectedMessage implements InternalMessage {
    private final InetSocketAddress address;

    public PlayerDisconnectedMessage(InetSocketAddress address) {
        this.address = address;
    }

    public InetSocketAddress getAddress() {
        return address;
    }
}
