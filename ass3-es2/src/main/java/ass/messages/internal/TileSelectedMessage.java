package ass.messages.internal;

public class TileSelectedMessage implements InternalMessage {

	public final int row;
	public final int column;

	public TileSelectedMessage(final int row, final int column) {
		this.row = row;
		this.column = column;
	}
}
