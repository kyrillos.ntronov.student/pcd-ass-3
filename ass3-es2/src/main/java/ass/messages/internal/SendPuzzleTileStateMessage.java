package ass.messages.internal;

import ass.messages.data.TilesList;

public class SendPuzzleTileStateMessage implements InternalMessage {

    private final TilesList tilesList;

    public SendPuzzleTileStateMessage(TilesList tilesList) {
        this.tilesList = tilesList;
    }

    public TilesList getTilesList() {
        return tilesList;
    }
}
