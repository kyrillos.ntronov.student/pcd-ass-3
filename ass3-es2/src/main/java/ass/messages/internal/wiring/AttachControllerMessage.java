package ass.messages.internal.wiring;

import akka.actor.ActorRef;
import ass.messages.internal.InternalMessage;

public class AttachControllerMessage implements InternalMessage {
    private final ActorRef controller;

    public AttachControllerMessage(final ActorRef controller) {
        this.controller = controller;
    }

    public ActorRef getController() {
        return controller;
    }
}
