package ass.messages.data;

import ass.view.data.Tile;

import java.util.List;

public class TilesList {

    private List<Tile> tiles;
    private List<Tile> selectedTiles;

    public TilesList() {
    }

    public TilesList(List<Tile> tiles, List<Tile> selectedTiles) {
        this.tiles = tiles;
        this.selectedTiles = selectedTiles;
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public void setTiles(List<Tile> tiles) {
        this.tiles = tiles;
    }

    public List<Tile> getSelectedTiles() {
        return selectedTiles;
    }

    public void setSelectedTiles(List<Tile> selectedTiles) {
        this.selectedTiles = selectedTiles;
    }
}
