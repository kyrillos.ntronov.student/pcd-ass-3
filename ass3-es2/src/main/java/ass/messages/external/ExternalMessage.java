package ass.messages.external;

/**
 * General object which represent all the type of messages sent between different controller instances.
 */
public interface ExternalMessage {
	/**
	 * Send Format must be a string format which starts with the type string of the message (example: 'PLAYER_JOINED') and a slash, 
	 * followed by the arguments divided by a slash each.
	 */
	String toSendFormat();
}
