package ass.messages.external.players;

import ass.messages.external.ExternalMessage;

/**
 * The response to a request to join a game.
 * The request should be accepted most of the times.
 */
public class PlayerJoinResponseMessage implements ExternalMessage {

	public static final String TYPE_STRING = "PLAYER_JOIN_RESPONSE";

	private final boolean accepted;

	public PlayerJoinResponseMessage(final boolean accepted) {
		this.accepted = accepted;
	}
	
	public boolean isAccepted() {
		return this.accepted;
	}

	@Override
	public String toSendFormat() {
		return TYPE_STRING + "/" + this.accepted;
	}

}
