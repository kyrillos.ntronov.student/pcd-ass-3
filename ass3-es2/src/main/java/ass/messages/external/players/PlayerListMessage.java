package ass.messages.external.players;

import ass.addressconverter.AddressConverter;
import ass.messages.external.ExternalMessage;
import org.jetbrains.annotations.NotNull;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.stream.Collectors;

public class PlayerListMessage implements ExternalMessage {

    public final static String TYPE_STRING = "PLAYER_LIST";

    public final List<InetSocketAddress> addresses;

    public PlayerListMessage(@NotNull final List<InetSocketAddress> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toSendFormat() {
        return TYPE_STRING + "/" + addresses.stream().map(AddressConverter::stringFromAddress).collect(Collectors.joining("/"));
    }
}
