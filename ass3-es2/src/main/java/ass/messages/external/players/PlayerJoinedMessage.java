package ass.messages.external.players;

import ass.messages.external.ExternalMessage;

import java.net.InetSocketAddress;

/**
 * A message for notifying other players about a new joined player.
 */
public class PlayerJoinedMessage implements ExternalMessage {

	public static final String TYPE_STRING = "PLAYER_JOINED";

	private final InetSocketAddress socketAddress;

	public PlayerJoinedMessage(final String hostname, final int port) {
		this.socketAddress = new InetSocketAddress(hostname, port);
	}

	public PlayerJoinedMessage(final InetSocketAddress address) {
		this.socketAddress = address;
	}

	public InetSocketAddress getSocketAddress() {
		return this.socketAddress;
	}

	@Override
	public String toSendFormat() {
		return TYPE_STRING + "/" + socketAddress.getHostName() + ":" + socketAddress.getPort();
	}

}
