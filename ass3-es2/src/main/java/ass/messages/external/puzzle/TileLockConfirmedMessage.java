package ass.messages.external.puzzle;

import ass.messages.external.ExternalMessage;

public class TileLockConfirmedMessage implements ExternalMessage {

    public static final String TYPE_STRING = "TILE_LOCK_CONFIRM";

    public final int row;
    public final int column;

    public TileLockConfirmedMessage(int row, int column) {
        this.row = row;
        this.column = column;
    }

    @Override
    public String toSendFormat() {
        return TYPE_STRING + "/" + row + "/" + column;
    }

}
