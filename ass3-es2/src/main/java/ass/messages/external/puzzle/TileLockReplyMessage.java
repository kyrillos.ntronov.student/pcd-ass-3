package ass.messages.external.puzzle;

import ass.addressconverter.AddressConverter;
import ass.messages.external.ExternalMessage;

import java.net.InetSocketAddress;

/**
 * A response to a tile lock request.
 */
public class TileLockReplyMessage implements ExternalMessage {

	public static final String TYPE_STRING = "TILE_LOCK_RES";

	public final InetSocketAddress responderAddress;
	public final int timestamp;

	public final int row;
	public final int col;

	public TileLockReplyMessage(final InetSocketAddress address, int timestamp, final int row, final int col) {
		this.responderAddress = address;
		this.timestamp = timestamp;
		this.row = row;
		this.col = col;
	}

	@Override
	public String toSendFormat() {
		return TYPE_STRING + "/" + timestamp + "/" + AddressConverter.stringFromAddress(responderAddress) + "/" + row + "/" + col;
	}

}
