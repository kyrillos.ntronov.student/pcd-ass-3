package ass.messages.external.puzzle;

import ass.messages.external.ExternalMessage;

/**
 * A message to notify that the puzzle has been completed.
 */
public class PuzzleCompletedMessage implements ExternalMessage {

	public final static String TYPE_STRING = "PUZZLE_COMPLETED"; 
	
	@Override
	public String toSendFormat() {
		return TYPE_STRING;
	}

}
