package ass.node;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import ass.addressconverter.AddressConverter;
import ass.config.NodeConfig;
import ass.log.Logger;
import ass.messages.external.ExternalMessage;
import ass.messages.external.players.PlayerJoinRequestMessage;
import ass.messages.external.players.PlayerJoinedMessage;
import ass.messages.external.players.PlayerLeftMessage;
import ass.messages.external.players.PlayerListMessage;
import ass.messages.external.puzzle.*;
import ass.messages.internal.*;
import ass.messages.internal.netcom.*;
import ass.messages.internal.selection.ViewSelectTileRequestMessage;
import ass.messages.internal.wiring.AttachControllerMessage;
import ass.messages.internal.wiring.AttachViewMessage;
import ass.model.cs.LogicalClock;
import ass.model.cs.TilesCriticalSectionsManager;
import ass.netcom.listener.ListenerActor;

public class Node extends AbstractActor {

    private final Logger log;

    private final NodeConfig config;
    private final ActorRef netSender;
    private ActorRef viewActor;

    private NodeState nodeState;
    private List<InetSocketAddress> knownNodes;
    private InetSocketAddress localAddress;

    private final LogicalClock logicalClock;
    private final TilesCriticalSectionsManager criticalSectionsManager;

    public Node(NodeConfig config, final ActorRef netSender) {
        this.config = config;
        this.log = new Logger("ACTOR " + config.getPort(), this.getClass());
        this.netSender = netSender;
        this.netSender.tell(new AttachControllerMessage(getSelf()), getSelf());
        this.viewActor = null;
        this.nodeState = NodeState.UNJOINABLE;
        this.knownNodes = new ArrayList<>();
        this.localAddress = null;
        this.logicalClock = new LogicalClock();
        this.criticalSectionsManager = new TilesCriticalSectionsManager();
        getContext().actorOf(Props.create(ListenerActor.class, () -> new ListenerActor(getSelf(), logicalClock, "localhost", config.getPort())), "net-listener");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
        		.match(ListenerBoundMessage.class, msg -> {
        			log.info("Controller online @ {}", msg.localAddress);
        			this.localAddress = msg.localAddress;
        			this.nodeState = NodeState.JOINABLE;
        		})
                .match(PlayerDisconnectedMessage.class, this::handlePlayerDisconnected)
                .match(PlayerJoinRequestMessage.class, this::handleJoinRequest)
                .match(PlayerJoinedMessage.class, this::handleJoinNotification)
                .match(PlayerLeftMessage.class, this::handlePlayerLeftMessage)
                .match(PlayerListMessage.class, this::handlePlayerList)
                .match(ConnectToMessage.class, msg -> this.handleConnectTo(msg.connectString))

                .match(AttachViewMessage.class, msg -> this.viewActor = msg.viewActor)

                .match(TileSelectedMessage.class, this::handleTileSelected)
                .match(TileLockRequestMessage.class, this::handleTileLockRequest)
                .match(TileLockReplyMessage.class, this::handleTileLockReply)

                .match(PuzzleTilesStateMessage.class, this::handlePuzzleStateUpdate)
                .match(SendPuzzleTileStateMessage.class, this::handleSendPuzzleStateUpdate)
                .match(InternalPuzzleCompletedMessage.class, this::handleInternalPuzzleCompleted)
                .match(PuzzleCompletedMessage.class, this::handlePuzzleCompleted)
                .build();
    }

    // When a site Si wants to enter the critical section, it send a timestamped REQUEST message to all other sites.
    private void handleTileSelected(TileSelectedMessage message) {
        if (knownNodes.isEmpty()) {
            this.viewActor.tell(new ViewSelectTileRequestMessage(message.row, message.column), getSelf());
        } else {
            criticalSectionsManager.makeNewRequest(knownNodes, message.row, message.column, () -> {
                // Site Si enters the critical section if it has received the REPLY message from all other sites.
                viewActor.tell(new ViewSelectTileRequestMessage(message.row, message.column), getSelf());
                // Upon exiting site Si sends REPLY message to all the deferred requests.
                broadcastToAll(new TileLockReplyMessage(this.localAddress, logicalClock.getTimestamp(), message.row, message.column));
            });
            broadcastToAll(new TileLockRequestMessage(this.localAddress, logicalClock.getTimestamp(), message.row, message.column));
        }
    }

    // When a site Sj receives a REQUEST message from site Si, It sends a REPLY message to site Si if and only if
    // Site Sj is neither requesting nor currently executing the critical section.
    // In case Site Sj is requesting, the timestamp of Site Si‘s request is smaller than its own request.
    //
    // Otherwise the request is deferred by site Sj.
    private void handleTileLockRequest(TileLockRequestMessage message) {
        if (criticalSectionsManager.isLockAvailable(message.row, message.column)) {
            sendExternalMessageTo(message.requesterAddress, new TileLockReplyMessage(this.localAddress, logicalClock.getTimestamp(), message.row, message.column));
        }
    }

    // Received Reply for current CS pending section
    private void handleTileLockReply(TileLockReplyMessage message) {
        criticalSectionsManager.registerReply(message.responderAddress, message.row, message.col);
    }

    private void handlePuzzleStateUpdate(PuzzleTilesStateMessage message) {
        viewActor.tell(message, getSender());
    }

    private void handleSendPuzzleStateUpdate(SendPuzzleTileStateMessage message) {
        PuzzleTilesStateMessage externalMessage = new PuzzleTilesStateMessage(message.getTilesList());
        broadcastToAll(externalMessage);
    }

    private void handlePlayerDisconnected(final PlayerDisconnectedMessage message) {
        this.removePlayer(message.getAddress());
        this.broadcastToAll(new PlayerLeftMessage(message.getAddress()));
    }

    private void handlePlayerLeftMessage(final PlayerLeftMessage message) {
        this.removePlayer(message.getSocketAddress());
    }

    private void removePlayer(final InetSocketAddress address) {
        this.log.debug("Removing player at {}:{}", address.getHostString(), address.getPort());
        this.knownNodes.remove(address);
    }

    private void handleConnectTo(final String address) {
        final Optional<InetSocketAddress> socketAddress = AddressConverter.addressFromString(address);
        if (socketAddress.isPresent()) {
            this.sendExternalMessageTo(socketAddress.get(), new PlayerJoinRequestMessage(this.localAddress));
        } else {
            log.error("Invalid address");
        }
    }
    
    private void handleJoinRequest(final PlayerJoinRequestMessage message) {
    	log.debug(message.toSendFormat());
    	log.debug("Received PLAYER_JOIN_REQ message");
		if (this.nodeState == NodeState.UNJOINABLE && knownNodes.size() < config.getMaxPlayers()) {
			log.debug("Cannot join new player");
		} else {
			log.debug("Can join new player");
			this.broadcastToAll(new PlayerJoinedMessage(message.getSocketAddress()));
            final List<InetSocketAddress> hostList = new ArrayList<>(knownNodes);
            hostList.add(this.localAddress);
            this.sendExternalMessageTo(message.getSocketAddress(), new PlayerListMessage(hostList));
			knownNodes.add(message.getSocketAddress());
            viewActor.tell(new PlayerConnectedMessage(message.getSocketAddress().toString(), knownNodes), getSender());
		}
    }
    
    private void handleJoinNotification(final PlayerJoinedMessage message) {
    	log.debug("Player joined");
    	this.knownNodes.add(message.getSocketAddress());
    }

    private void handlePlayerList(final PlayerListMessage message) {
        message.addresses.forEach(address -> {
            if (!knownNodes.contains(address)) {
                knownNodes.add(address);
            }
        });
    }

    private void handleInternalPuzzleCompleted(InternalPuzzleCompletedMessage message) {
        broadcastToAll(new PuzzleCompletedMessage());
    }

    private void handlePuzzleCompleted(PuzzleCompletedMessage message) {
        viewActor.tell(new InternalPuzzleCompletedMessage(), getSender());
    }

    private void broadcastToAll(final ExternalMessage message) {
        for (InetSocketAddress knownNode : knownNodes) {
            sendExternalMessageTo(knownNode, message);
        }
    }

    private void sendExternalMessageTo(final InetSocketAddress address, final ExternalMessage message) {
        this.netSender.tell(new SendExternalMessage(address, message), getSelf());
    }


    @Override
    public String toString() {
        return "Node{" +
                "config=" + config +
                ", nodeState=" + nodeState +
                '}';
    }

}
