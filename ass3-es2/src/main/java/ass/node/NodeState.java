package ass.node;

public enum NodeState {

    UNJOINABLE,
    JOIN_PENDING,
    JOINABLE,
    ACCEPT_PENDING,
    HEARTBEAT_REQUESTED

}
