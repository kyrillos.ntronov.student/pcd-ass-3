package ass.view.data;

import java.util.Objects;

public class Tile implements Comparable<Tile> {

    private int imageIndex;
    private int originalPosition;
    private int currentPosition;

    public Tile() {
    }

    public Tile(final int imageIndex, final int originalPosition, final int currentPosition) {
        this.imageIndex = imageIndex;
        this.originalPosition = originalPosition;
        this.currentPosition = currentPosition;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public int getOriginalPosition() {
        return originalPosition;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    @Override
    public int compareTo(Tile other) {
        return Integer.compare(this.currentPosition, other.currentPosition);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile tile = (Tile) o;
        return imageIndex == tile.imageIndex && originalPosition == tile.originalPosition && currentPosition == tile.currentPosition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(imageIndex, originalPosition, currentPosition);
    }
}
