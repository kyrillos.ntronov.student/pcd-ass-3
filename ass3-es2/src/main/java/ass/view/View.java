package ass.view;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import ass.config.NodeConfig;
import ass.messages.data.TilesList;
import ass.messages.external.puzzle.PuzzleTilesStateMessage;
import ass.messages.internal.InternalPuzzleCompletedMessage;
import ass.messages.internal.SendPuzzleTileStateMessage;
import ass.messages.internal.TileSelectedMessage;
import ass.messages.internal.netcom.ConnectToMessage;
import ass.messages.internal.netcom.PlayerConnectedMessage;
import ass.messages.internal.selection.ViewSelectTileRequestMessage;
import ass.model.SelectionManager;
import ass.model.TilesGrid;
import ass.view.data.Tile;
import ass.view.data.TileButton;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class View extends AbstractActor {

    private final ActorRef controller;

    private final TilesGrid tilesGrid;
    private final NodeConfig config;
    private final SelectionManager selectionManager;

    private JFrame frame;
    private JPanel board;

    public View(TilesGrid tilesGrid, NodeConfig config, ActorRef controller) {
        this.controller = controller;
        this.selectionManager = new SelectionManager();
        this.tilesGrid = tilesGrid;
        this.config = config;
        initFrame();
        initTiles();
        initPanels();
        paintPuzzle();

        frame.setVisible(true);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(PuzzleTilesStateMessage.class, this::handleStateUpdate)
                .match(InternalPuzzleCompletedMessage.class, this::handlePuzzleCompleted)
                .match(PlayerConnectedMessage.class, this::onPlayerConnected)
                .match(ViewSelectTileRequestMessage.class, this::handleSelectTile)
                .build();
    }

    private void handleStateUpdate(PuzzleTilesStateMessage message) {
        tilesGrid.getTiles().clear();
        tilesGrid.getSelectedTiles().clear();
        tilesGrid.getTiles().addAll(message.getTiles().getTiles());
        tilesGrid.getSelectedTiles().addAll(message.getTiles().getSelectedTiles());
        if(tilesGrid.getCurrentlySelectedTile() != null) {
            tilesGrid.getSelectedTiles().remove(tilesGrid.getCurrentlySelectedTile());
        }
        paintPuzzle();
    }

    public void requestConnect(String connectionString) {
        controller.tell(new ConnectToMessage(connectionString), this.getSender());
    }

    private void initFrame() {
        frame = new JFrame();
        frame.setTitle("Puzzle");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initTiles() {
        try {
            createTiles();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(frame, "Could not load image", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void initPanels() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        this.board = initBoardPanel();
        JPanel connect = initConnectPanel();
        JPanel players = initPlayersPanel();
        mainPanel.add(board);
        mainPanel.add(connect);
        mainPanel.add(players);
        frame.getContentPane().add(mainPanel, BorderLayout.CENTER);
    }

    private JPanel initBoardPanel() {
        JPanel puzzlePanel = new JPanel();
        puzzlePanel.setBorder(BorderFactory.createLineBorder(Color.gray));
        puzzlePanel.setLayout(new GridLayout(tilesGrid.getRows(), tilesGrid.getColumns(), 0, 0));
        return puzzlePanel;
    }

    private JPanel initConnectPanel() {
        JPanel connectPanel = new JPanel();
        connectPanel.setBorder(BorderFactory.createLineBorder(Color.gray));
        connectPanel.setLayout(new BoxLayout(connectPanel, BoxLayout.X_AXIS));

        JTextField addressBox = new JTextField();
        addressBox.setText("type ip address");

        JButton connectButton = new JButton("Connect");
        connectButton.addActionListener(l -> requestConnect(addressBox.getText()));

        connectPanel.add(addressBox);
        connectPanel.add(connectButton);
        return connectPanel;
    }

    private JPanel initPlayersPanel() {
        JPanel playersPanel = new JPanel();
        playersPanel.setBorder(BorderFactory.createLineBorder(Color.gray));
        playersPanel.setLayout(new BoxLayout(playersPanel, BoxLayout.Y_AXIS));

        JTextArea playersTextArea = new JTextArea();
        playersTextArea.setBackground(Color.black);
        playersTextArea.setForeground(Color.white);
        playersTextArea.setText("localhost@" + config.getPort());

        playersPanel.add(playersTextArea);
        return playersPanel;
    }

    private void createTiles() throws IOException {
        final BufferedImage image;

        File imageFile = new File(tilesGrid.getImagePath());
        image = ImageIO.read(imageFile);

        final int imageWidth = image.getWidth(null);
        final int imageHeight = image.getHeight(null);

        int position = 0;
        int rows = tilesGrid.getRows();
        int columns = tilesGrid.getColumns();

        final List<Integer> randomPositions = new ArrayList<>();
        IntStream.range(0, rows * columns).forEach(randomPositions::add);
        Collections.shuffle(randomPositions);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                final Image imagePortion = frame.createImage(new FilteredImageSource(image.getSource(),
                        new CropImageFilter(j * imageWidth / columns,
                                i * imageHeight / rows,
                                (imageWidth / columns),
                                imageHeight / rows)));

                tilesGrid.getImageCache().add(position, imagePortion);
                tilesGrid.getTiles().add(new Tile(position, position, randomPositions.get(position)));
                position++;
            }
        }
    }

    private void paintPuzzle() {
        board.removeAll();
        tilesGrid.getTiles().stream()
                .sorted()
                .collect(Collectors.toList())
                .forEach(this::paintTile);
        frame.pack();
        //frame.setLocationRelativeTo(null);
    }

    private void paintTile(Tile tile) {
        final TileButton btn = new TileButton(tile, tilesGrid.getImageCache().get(tile.getImageIndex()));
        board.add(btn);
        btn.setEnabled(!tilesGrid.getSelectedTiles().contains(tile) || tilesGrid.getCurrentlySelectedTile() == tile);
        paintBorders(tile, btn);

        btn.addActionListener(actionListener -> {
            if (!tilesGrid.getSelectedTiles().contains(tile)) {
                final int tilePosition = tile.getCurrentPosition();
                final int tileRow = tilePosition / tilesGrid.getColumns();
                final int tileColumn = tilePosition % tilesGrid.getColumns();
                this.controller.tell(new TileSelectedMessage(tileRow, tileColumn), getSelf());
                tilesGrid.setCurrentlySelectedTile(tile);
            } else {
                selectionManager.deselectTile();
                tilesGrid.getSelectedTiles().remove(tile);
                tilesGrid.setCurrentlySelectedTile(null);
                controller.tell(new SendPuzzleTileStateMessage(new TilesList(tilesGrid.getTiles(), tilesGrid.getSelectedTiles())), getSender());
            }
            paintBorders(tile, btn);
        });
    }

    private void paintBorders(Tile tile, TileButton btn) {
        if (tile.equals(tilesGrid.getCurrentlySelectedTile())) {
            btn.setBorder(BorderFactory.createLineBorder(Color.blue));
        } else if (tilesGrid.getSelectedTiles().contains(tile)) {
            btn.setBorder(BorderFactory.createLineBorder(Color.red));
        } else {
            btn.setBorder(BorderFactory.createLineBorder(Color.gray));
        }
    }

    // This is executed in critical section
    private void handleSelectTile(ViewSelectTileRequestMessage message) {
        final int totalColumns = tilesGrid.getColumns();
        final Tile selectedTile = tilesGrid.getTiles().stream()
                .sorted()
                .collect(Collectors.toList())
                .get(message.row * totalColumns + message.col);

        tilesGrid.getSelectedTiles().add(selectedTile);

        selectionManager.selectTile(selectedTile, newTile -> {
            tilesGrid.getSelectedTiles().remove(selectedTile);
            tilesGrid.getSelectedTiles().remove(newTile);
            tilesGrid.setCurrentlySelectedTile(null);
            paintPuzzle();
            controller.tell(new SendPuzzleTileStateMessage(new TilesList(tilesGrid.getTiles(), tilesGrid.getSelectedTiles())), getSender());
            checkSolution();
        });
    }

    private void handlePuzzleCompleted(InternalPuzzleCompletedMessage message) {
        puzzleCompleted();
    }

    private void checkSolution() {
        if (puzzleCompleted()) {
            controller.tell(new InternalPuzzleCompletedMessage(), getSender());
        }
    }

    private boolean puzzleCompleted() {
        if (tilesGrid.getTiles().stream().allMatch(this::isTileInRightPlace)) {
            this.showInformationDialog("Puzzle completed!");
            return true;
        } else {
            return false;
        }
    }

    private boolean isTileInRightPlace(Tile tile) {
        return tile.getCurrentPosition() == tile.getOriginalPosition();
    }

    private void onPlayerConnected(PlayerConnectedMessage message) {
        this.showInformationDialog("Player " + message.getPlayerName() + " connected!");
        TilesList tilesList = new TilesList(tilesGrid.getTiles(), tilesGrid.getSelectedTiles());
        SendPuzzleTileStateMessage externalMessage = new SendPuzzleTileStateMessage(tilesList);
        controller.tell(externalMessage, getSender());
    }

    private void showInformationDialog(final String message) {
        JOptionPane pane = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE);
        JDialog dialog = pane.createDialog(frame, "");
        dialog.setModal(false);
        dialog.setVisible(true);
    }

}
