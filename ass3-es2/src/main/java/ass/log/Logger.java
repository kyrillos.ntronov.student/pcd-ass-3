package ass.log;

import org.slf4j.LoggerFactory;

public class Logger {

    private final String nodeName;
    private final org.slf4j.Logger log;

    public Logger(String nodeName, Class<?> clazz) {
        this.nodeName = nodeName;
        this.log = LoggerFactory.getLogger(clazz);
    }

    public void info(String template, Object... params) {
        String logTemplate = "[" + nodeName + "] " + template;
        log.info(logTemplate, params);
    }

    public void error(String template, Object... params) {
        String logTemplate = "[" + nodeName + "] " + template;
        log.error(logTemplate, params);
    }

    public void debug(String template, Object... params) {
        String logTemplate = "[" + nodeName + "] " + template;
        log.debug(logTemplate, params);
    }

}
