package ass.netcom.sender;

import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.io.Tcp;
import akka.io.Tcp.CommandFailed;
import akka.io.Tcp.Connected;
import akka.io.Tcp.ConnectionClosed;
import akka.io.TcpMessage;
import akka.util.ByteString.ByteStrings;
import ass.log.Logger;
import ass.messages.external.ExternalMessage;
import ass.messages.internal.wiring.AttachControllerMessage;
import ass.messages.internal.InternalMessage;
import ass.messages.internal.netcom.PlayerDisconnectedMessage;
import ass.messages.internal.netcom.SendExternalMessage;

public class SenderActor extends AbstractActor {

	private final Logger log;
	private ActorRef controller = null;

	private final Queue<InternalMessage> messageQueue;
	
	public SenderActor(int port) {
		this.log = new Logger("ACTOR " + port, this.getClass());
		this.messageQueue = new LinkedList<>();
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(AttachControllerMessage.class, msg -> {
					this.controller = msg.getController();
				})
				.match(SendExternalMessage.class, msg -> sendExternalMessage(msg.socketAddress, msg.msg))
				.match(CommandFailed.class, msg -> log.debug(msg.causedByString()))
				.match(ConnectionClosed.class, msg -> {
					log.info("Connection closed");
					if (!messageQueue.isEmpty()) {
						getSelf().tell(messageQueue.remove(), ActorRef.noSender());
					}
				})
				.build();
	}

	private void sendExternalMessage(final InetSocketAddress socketAddress, final ExternalMessage msg) {
		final ActorRef tcpManager = Tcp.get(getContext().getSystem()).manager();
		getContext().become(this.waitForConnection(tcpManager, msg));
		tcpManager.tell(TcpMessage.connect(socketAddress), getSelf());
	}

	private Receive waitForConnection(final ActorRef tcpManager, final ExternalMessage msg) {
		return receiveBuilder()
				.match(Connected.class, response -> {
					log.debug("Sender connected to listener @ " + response.remoteAddress().toString());
					getSender().tell(TcpMessage.register(getSelf()), getSelf());
					getSender().tell(TcpMessage.write(ByteStrings.fromString(msg.toSendFormat())), getSelf());
					getContext().become(createReceive());
					getSender().tell(TcpMessage.close(), getSelf());
				})
				.match(CommandFailed.class, failedMessage -> {
					log.debug(failedMessage.causedByString());
					if (failedMessage.cmd() instanceof Tcp.Connect) {
						final Tcp.Connect connectMessage = (Tcp.Connect) failedMessage.cmd();
						log.debug("Failed to connect to {}:{}", connectMessage.remoteAddress().getHostString(), connectMessage.remoteAddress().getPort());
						if (Objects.nonNull(this.controller)) {
							this.controller.tell(new PlayerDisconnectedMessage(connectMessage.remoteAddress()), getSelf());
						} else {
							log.info("No controller set. Cannot send a Player Disconnected Message.");
						}
					}
					getContext().become(createReceive());
				})
				.match(InternalMessage.class, this.messageQueue::add)
				.build();
	}

}
