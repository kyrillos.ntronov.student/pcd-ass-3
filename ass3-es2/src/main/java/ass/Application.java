package ass;

import ass.config.NodeConfig;
import ass.config.NodeConfigLoader;
import ass.messages.internal.wiring.AttachViewMessage;
import ass.model.TilesGrid;
import ass.netcom.sender.SenderActor;
import ass.node.Node;
import ass.view.View;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import java.io.IOException;


public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	private static ActorRef controller;
	private static ActorRef view;

    static {
    	BasicConfigurator.configure();
    }

	private static NodeConfig config;
	
	private static final ActorSystem actorSystem = ActorSystem.create("puzzle-actors");

	public static void main(final String[] args) {
		loadConfig(args[0]);
		startNodeController();
		loadGui();
	}

	private static void loadConfig(String port) {
		try {
			NodeConfigLoader loader = new NodeConfigLoader(port);
			config = loader.loadProperties();
		} catch (IOException e) {
			log.error("Cannot Load Config {}", e.getMessage(), e);
			throw new IllegalStateException("Cannot Load Config");
		}
	}

	private static void loadGui() {
		view = actorSystem.actorOf(Props.create(View.class, () -> new View(new TilesGrid(config), config, controller)));
		controller.tell(new AttachViewMessage(view), ActorRef.noSender());
	}
	
	private static void startNodeController() {
		final ActorRef netSender = actorSystem.actorOf(Props.create(SenderActor.class, () -> new SenderActor(config.getPort())));
		controller = actorSystem.actorOf(Props.create(Node.class, () -> new Node(config, netSender)));
		log.info("Controller started");
	}
}
