package ass.addressconverter;

import ass.log.Logger;
import org.jetbrains.annotations.NotNull;

import java.net.InetSocketAddress;
import java.util.Optional;

public class AddressConverter {
    private final static Logger LOG = new Logger("AddressConverter", AddressConverter.class);

    private AddressConverter() {}

    /**
     * Parses a string into a socket address.
     * @param addressString Must be in the format "<hostname>:<port>".
     * @return An InetSocketAddress representing the given address.
     */
    public static Optional<InetSocketAddress> addressFromString(@NotNull final String addressString) {
        final String[] splitAddress = addressString.split(":");
        if (splitAddress.length == 2) {
            final String hostname = splitAddress[0];
            final String port = splitAddress[1];
            try {
                return Optional.of(new InetSocketAddress(hostname, Integer.parseInt(port)));
            } catch (NumberFormatException e) {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    /**
     * Create a InetSocketAddress from the given hostname and port.
     * @param hostname The hostname of the address
     * @param port The port of the socket
     * @return An InetSocketAddress representing the given address.
     */
    public static InetSocketAddress addressFromHostnameAndPort(@NotNull final String hostname, final int port) {
        return new InetSocketAddress(hostname, port);
    }

    /**
     * Returns a literal string with the hostname and port of the given address.
     * @param address The address to stringify.
     * @return The string in the format "<hostname>:<port>"
     */
    public static String stringFromAddress(@NotNull final InetSocketAddress address) {
        return address.getHostName() + ":" + address.getPort();
    }
}
