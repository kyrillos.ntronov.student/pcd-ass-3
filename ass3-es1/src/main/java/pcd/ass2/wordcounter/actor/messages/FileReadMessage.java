package pcd.ass2.wordcounter.actor.messages;

import java.io.File;

public class FileReadMessage {

    private final File file;

    public FileReadMessage(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}
