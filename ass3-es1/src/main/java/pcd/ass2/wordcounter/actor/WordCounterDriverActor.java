package pcd.ass2.wordcounter.actor;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import pcd.ass2.wordcounter.actor.messages.FileReadMessage;
import pcd.ass2.wordcounter.actor.messages.FolderSearchMessage;
import pcd.ass2.wordcounter.actor.messages.PageWordCountMessage;
import pcd.ass2.wordcounter.actor.messages.WordCountResultMessage;
import pcd.ass2.wordcounter.service.WordCounterService;
import pcd.ass2.wordcounter.structures.data.WordCountResult;

/**
 * Coordinates Word Counter Task results by creating new actors and rasing messages,
 * then finally putting the results into the swing worker's processing queue
 */
public class WordCounterDriverActor extends AbstractActor {

    public static final String PAUSE_MESSAGE = "PAUSE";
    public static final String RESUME_MESSAGE = "RESUME";
    public static final String CHILD_DONE_MESSAGE = "DONE";

    private final List<String> excludedWords;
    private final BlockingQueue<WordCountResult> blockingWordCountQueue;

    private final List<ActorRef> children;
    private final WordCounterService masterService;

    private int activeChildren = 0;

    public WordCounterDriverActor(List<String> excludedWords, BlockingQueue<WordCountResult> blockingWordCountQueue, WordCounterService service) {
        this.excludedWords = excludedWords;
        this.blockingWordCountQueue = blockingWordCountQueue;
        this.masterService = service;

        this.children = new LinkedList<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(String.class, this::matchStringMessage)
                .match(FolderSearchMessage.class, this::matchFolderSearchMessage)
                .match(FileReadMessage.class, this::matchFileReadMessage)
                .match(PageWordCountMessage.class, this::matchPageWordCountMessage)
                .match(WordCountResultMessage.class, this::matchWordCountResultMessage)
                .build();
    }

    private void matchStringMessage(String message) {
        switch (message) {
            case PAUSE_MESSAGE: children.forEach(c -> c.tell(PAUSE_MESSAGE, sender()));break;
            case RESUME_MESSAGE: children.forEach(c -> c.tell(RESUME_MESSAGE, sender())); break;
            case CHILD_DONE_MESSAGE: activeChildren--; if (activeChildren <= 0) { this.masterService.done(); } break;
        }
    }

    private void matchFolderSearchMessage(FolderSearchMessage message) {
        ActorRef ref = context().actorOf(Props.create(WordCounterFolderPDFSearchActor.class, () -> new WordCounterFolderPDFSearchActor(message.getBaseDirectory())));
        children.add(ref);
        activeChildren++;
    }

    private void matchFileReadMessage(FileReadMessage message) {
        ActorRef ref = context().actorOf(Props.create(WordCounterFileReadActor.class, () -> new WordCounterFileReadActor(message.getFile())));
        children.add(ref);
        activeChildren++;
    }

    private void matchPageWordCountMessage(PageWordCountMessage message) {
        ActorRef ref = context().actorOf(Props.create(WordCounterPageWordCountActor.class, () -> new WordCounterPageWordCountActor(message.getPage(), excludedWords)));
        children.add(ref);
        activeChildren++;
    }

    private void matchWordCountResultMessage(WordCountResultMessage message) {
        blockingWordCountQueue.add(message.getResult());
    }

}
