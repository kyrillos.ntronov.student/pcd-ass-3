package pcd.ass2.wordcounter.actor;

import java.io.File;
import java.util.Objects;

import akka.actor.ActorRef;
import pcd.ass2.logging.Log;
import pcd.ass2.wordcounter.actor.messages.FileReadMessage;
import pcd.ass2.wordcounter.actor.messages.FolderSearchMessage;

public class WordCounterFolderPDFSearchActor extends PausableActor {

    private static final String NAME = "READ_FOLDER";

    private final ActorRef master;
    private final File baseDirectory;

    public WordCounterFolderPDFSearchActor(File baseDirectory) {
        this.master = getContext().parent();
        this.baseDirectory = baseDirectory;

        start();
    }

    private void start() {
        for(File file : Objects.requireNonNull(baseDirectory.listFiles())) {
            waitIfPaused();
            processFile(file);
        }
        master.tell(WordCounterDriverActor.CHILD_DONE_MESSAGE, sender());
    }

    private void processFile(final File file) {
        if(file.isDirectory()) {
            Log.info("Directory found: " + file.getAbsolutePath(), NAME);
            processDirectory(file);
        } else if(isReadablePdfFile(file)) {
            Log.info("Readable pdf found: " + file.getAbsolutePath(), NAME);
            processPdf(file);
        }
    }

    private void processDirectory(final File file) {
        FolderSearchMessage message = new FolderSearchMessage(file);
        master.tell(message, sender());
    }

    private boolean isReadablePdfFile(final File file) {
        return file.isFile() && file.canRead() && file.getName().endsWith(".pdf");
    }

    private void processPdf(final File file) {
        FileReadMessage message = new FileReadMessage(file);
        master.tell(message, sender());
    }

}
