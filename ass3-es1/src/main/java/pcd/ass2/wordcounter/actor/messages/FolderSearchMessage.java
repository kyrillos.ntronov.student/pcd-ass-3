package pcd.ass2.wordcounter.actor.messages;

import java.io.File;

public class FolderSearchMessage {

    private final File baseDirectory;


    public FolderSearchMessage(File baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    public File getBaseDirectory() {
        return baseDirectory;
    }
}
