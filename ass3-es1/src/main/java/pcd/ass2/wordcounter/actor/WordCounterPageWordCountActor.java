package pcd.ass2.wordcounter.actor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import akka.actor.ActorRef;
import pcd.ass2.logging.Log;
import pcd.ass2.wordcounter.actor.messages.WordCountResultMessage;
import pcd.ass2.wordcounter.structures.data.Page;
import pcd.ass2.wordcounter.structures.data.WordCountResult;

public class WordCounterPageWordCountActor extends PausableActor {

    private static final String NAME = "PARSE_TEXT";

    private final ActorRef master;
    private final Page page;
    private final List<String> excludedWords;
    private final Map<String, Integer> countMap;

    public WordCounterPageWordCountActor(Page page, List<String> excludedWords) {
        this.master = getContext().parent();
        this.page = page;
        this.excludedWords = excludedWords;
        this.countMap = new HashMap<>();

        start();
    }

    private void start() {
        Map<String, Integer> wordCounts = countWordsInPage();
        master.tell(new WordCountResultMessage(new WordCountResult(wordCounts)), sender());
        master.tell(WordCounterDriverActor.CHILD_DONE_MESSAGE, sender());
    }

    private Map<String, Integer> countWordsInPage() {
        Log.info("Starting parsing page", NAME);
        for (String rawWord : page.getWords()) {
            String word = rawWord.toLowerCase();
            if (!word.isEmpty() && !excludedWords.contains(word)) {
                waitIfPaused();
                registerWordOccurence(word);
            }
        }
        Log.info("Finished parsing page", NAME);
        return countMap;
    }

    private void registerWordOccurence(String word) {
        Integer wordOccurencies = countMap.get(word);
        if (wordOccurencies != null) {
            countMap.put(word, wordOccurencies + 1);
        } else {
            countMap.put(word, 1);
        }
    }

}
