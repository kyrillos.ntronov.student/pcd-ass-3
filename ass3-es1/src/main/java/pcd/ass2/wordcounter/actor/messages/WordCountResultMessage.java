package pcd.ass2.wordcounter.actor.messages;

import pcd.ass2.wordcounter.structures.data.WordCountResult;

public class WordCountResultMessage {

    private final WordCountResult result;

    public WordCountResultMessage(WordCountResult result) {
        this.result = result;
    }

    public WordCountResult getResult() {
        return result;
    }
}
