package pcd.ass2.wordcounter.actor.messages;

import pcd.ass2.wordcounter.structures.data.Page;

public class PageWordCountMessage {

    private final Page page;

    public PageWordCountMessage(Page page) {
        this.page = page;
    }

    public Page getPage() {
        return page;
    }
}
