package pcd.ass2.wordcounter.actor;

import akka.actor.AbstractActor;

public abstract class PausableActor extends AbstractActor {

    private volatile boolean isPaused;

    public void setIsPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().match(String.class, msg -> {
            if(msg.equals(WordCounterDriverActor.PAUSE_MESSAGE)) {
                setIsPaused(true);
            } else if(msg.equals(WordCounterDriverActor.RESUME_MESSAGE)) {
                setIsPaused(false);
            }
        }).build();
    }

    protected void waitIfPaused() {
        // NOTE Akka actors are not pausable, does nothing
    }

}
