package pcd.ass2.wordcounter.service;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import pcd.ass2.gui.WordCounterParameters;
import pcd.ass2.logging.Log;
import pcd.ass2.wordcounter.actor.WordCounterDriverActor;
import pcd.ass2.wordcounter.actor.messages.FolderSearchMessage;
import pcd.ass2.wordcounter.structures.data.WordCountResult;
import pcd.ass2.wordcounter.structures.data.WordCounterEntry;
import pcd.ass2.wordcounter.structures.data.WordCounterMap;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collectors;

public class WordCounterService extends SwingWorker<List<WordCounterEntry>, List<WordCounterEntry>> {

    private final int coresNumber;
    private final int wordsNumber;
    private final File baseDirectory;
    private final File exclusionFile;

    private final List<String> excludedWords;
    private final WordCounterMap wordCountDictionary;

    private final ActorSystem actorSystem;
    private final BlockingQueue<WordCountResult> blockingWordCountQueue;

    private ActorRef driverActor;

    private boolean isPaused;

    private final JTextArea outputTextArea;
    private final JTextField wordCountField;
    private final JButton startButton;

    private Instant jobStart;

    public WordCounterService(final WordCounterParameters parameters, JTextArea outputTextArea,
                              JTextField wordCountField, JButton startButton) {
        this.wordsNumber = parameters.getMostFrequentWordsNumber();
        this.coresNumber = parameters.getVirtualCoresNumber();
        this.baseDirectory = parameters.getDirectory();
        this.exclusionFile = parameters.getExclusionFile();
        this.excludedWords = new LinkedList<>();

        this.actorSystem = ActorSystem.create("wordcounter-system");

        this.blockingWordCountQueue = new LinkedBlockingDeque<>();

        this.wordCountDictionary = new WordCounterMap(wordsNumber);
        this.outputTextArea = outputTextArea;
        this.wordCountField = wordCountField;
        this.startButton = startButton;
    }

    @Override
    public List<WordCounterEntry> doInBackground() throws Exception {
        if (driverActor != null && !isPaused) {
            Log.error("Cannot start another count while previous is still in progress!", "MAIN");
            return new ArrayList<>();
        } else {
            outputTextArea.setText("Reading files, please wait...");
            countWordOccurencies();
            return wordCountDictionary.getTopWords();
        }
    }

    @Override
    public void process(List<List<WordCounterEntry>> chunks) {
        if (!isPaused) {
            Log.info("PUBLISHING CHUNKS", "MAIN");
            outputTextArea.setText("");
            List<WordCounterEntry> chunk = chunks.get(chunks.size() - 1);
            for (WordCounterEntry entry : chunk) {
                outputTextArea.append(String.format("%s - %s %n", entry.getWord(), entry.getWordOccurency()));
            }
            wordCountField.setText(String.valueOf("Words Counted: " + wordCountDictionary.getWordsCounted()));
        }
    }

    @Override
    public void done() {
        actorSystem.stop(driverActor);
        outputTextArea.setText("-----Done Counting-----\n" + outputTextArea.getText());
        startButton.setEnabled(true);
    }

    public void pauseCount() {
        if (driverActor != null && !isPaused) {
            isPaused = true;
            driverActor.tell(WordCounterDriverActor.PAUSE_MESSAGE, ActorRef.noSender());
            Log.info("Computations paused", "MAIN");
        } else {
            Log.info("Cannot pause workers, no word count is in progress", "MAIN");
        }
    }

    public void resumeCount() {
        if (driverActor != null && isPaused) {
            isPaused = false;
            driverActor.tell(WordCounterDriverActor.RESUME_MESSAGE, ActorRef.noSender());
            Log.info("Computations resumed", "MAIN");
        } else {
            Log.info("Cannot resume workers, word count is in progress", "MAIN");
        }
    }

    private void countWordOccurencies() throws InterruptedException {
        resetCount();
        loadExclusionFile();
        loadFolderTask();
        processResults();
        terminateCount();
    }

    private void resetCount() {
        wordCountDictionary.clear();
        excludedWords.clear();
        jobStart = Instant.now();
        Log.info(String.format("Job started with %s workers", coresNumber), "MAIN");
    }


    private void loadFolderTask() {
        driverActor = actorSystem.actorOf(Props.create(WordCounterDriverActor.class, () -> new WordCounterDriverActor(excludedWords, blockingWordCountQueue, this)), "driver-actor");
        driverActor.tell(new FolderSearchMessage(baseDirectory), ActorRef.noSender());
    }

    private void loadExclusionFile() {
        if (exclusionFile != null) {
            try {
                String content = new String(Files.readAllBytes(Paths.get(exclusionFile.getPath())));
                excludedWords.addAll(Arrays.asList(content.split("\n")));
                List<String> tmp = excludedWords.stream().map(String::trim).map(String::toLowerCase)
                        .collect(Collectors.toList());
                excludedWords.clear();
                excludedWords.addAll(tmp);
                Log.info(String.format("Excluded words file found: %s", excludedWords), "MAIN");
            } catch (IOException e) {
                throw new IllegalStateException("IO Exception encountered: " + e);
            }
        } else {
            Log.info("Excluded words file was not provided", "MAIN");
        }
    }

    private void processResults() throws InterruptedException {
        while (!driverActor.isTerminated()) {
            WordCountResult result = blockingWordCountQueue.take();
            handleJobResult(result);
        }
    }

    private void handleJobResult(WordCountResult jobResult) {
        wordCountDictionary.registerWordOccurences(jobResult.getWordCounts());
        publish(wordCountDictionary.getTopWords());
    }

    private void terminateCount() {
        actorSystem.terminate();
        Instant countFinish = Instant.now();
        Log.info(String.format("Finished Counting Words in %s ms", Duration.between(jobStart, countFinish).toMillis()),
                "MAIN");
        Log.info("Output: counted " + wordCountDictionary.getWordsCounted() + " top words "
                + wordCountDictionary.getTopWords(), "MAIN");
    }

}
