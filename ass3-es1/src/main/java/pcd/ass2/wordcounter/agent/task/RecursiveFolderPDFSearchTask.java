package pcd.ass2.wordcounter.agent.task;

import java.io.File;

import pcd.ass2.logging.Log;

/**
 * Searches the provided base folder and its subfolders recursively for readable pdf files and creates
 * ReadPDFTasks subtasks. 
 * @author Cyrill
 *
 */
public class RecursiveFolderPDFSearchTask extends WordCounterTask<Void> {
	@Override
	protected Void compute() {
		return null;
	}

	/*private static final long serialVersionUID = -7960189301576268359L;
	
	private static final String NAME = "READ_FOLDER";
	
	private final File baseDirectory;
	
	private final transient ServiceTaskFactory taskFactory;
	
	public RecursiveFolderPDFSearchTask(final ServiceTaskFactory taskFactory, final File baseDirectory) {
		super();
		this.baseDirectory = baseDirectory;
		this.taskFactory = taskFactory;
	}
	
	@Override
	protected Void compute() {
		for(File file : baseDirectory.listFiles()) {
			waitIfPaused();
			processFile(file);
		}
		awaitForks();
		return null;
	}
		
	private void processFile(final File file) {
		if(file.isDirectory()) {
			Log.info("Directory found: " + file.getAbsolutePath(), NAME);
			processDirectory(file);
		} else if(isReadablePdfFile(file)) {
			Log.info("Readable pdf found: " + file.getAbsolutePath(), NAME);
			processPdf(file);
		}
	}
	
	private void processDirectory(final File file) {
		RecursiveFolderPDFSearchTask subFolderTask = taskFactory.recursiveFolderPDFSearchTask(file);
		getForks().add(subFolderTask);
		subFolderTask.fork();
	}
	
	private boolean isReadablePdfFile(final File file) {
		return file.isFile() && file.canRead() && file.getName().endsWith(".pdf");
	}
	
	private void processPdf(final File file) {
		ReadPDFTask readPdfTask = taskFactory.readPDFTask(file);
		getForks().add(readPdfTask);
		readPdfTask.fork();
	}*/
	
}
