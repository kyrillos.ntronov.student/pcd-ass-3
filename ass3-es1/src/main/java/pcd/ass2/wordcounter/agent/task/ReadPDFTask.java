package pcd.ass2.wordcounter.agent.task;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pcd.ass2.logging.Log;
import pcd.ass2.parser.PdfParser;
import pcd.ass2.parser.PdfParserException;
import pcd.ass2.wordcounter.structures.data.Page;

/**
 * Reads file and splits read text in pages creating a CountWordsTask for each
 * page.
 */
public class ReadPDFTask extends WordCounterTask<List<Page>> {
	@Override
	protected List<Page> compute() {
		return null;
	}

	/*private static final long serialVersionUID = 3945583394181755609L;

	private static final int PAGE_SIZE = 5000;

	private static final String NAME = "READ_PDF";

	private final String filePath;

	private final transient ServiceTaskFactory taskFactory;

	public ReadPDFTask(ServiceTaskFactory taskFactory, final File file) {
		this.filePath = file.getAbsolutePath();
		this.taskFactory = taskFactory;
	}

	@Override
	protected List<Page> compute() {
		try {
			List<Page> pages = processPages();
			awaitForks();
			return pages;
		} catch (PdfParserException e) {
			throw new IllegalStateException(e);
		}
	}

	private List<Page> processPages() throws PdfParserException {
		Log.info("Starting reading: " + filePath, NAME);
		List<Page> parsedPages = readPages(filePath);
		Log.info("Reading completed, added " + parsedPages.size() + " pages: " + filePath, NAME);
		return parsedPages;
	}

	private List<Page> readPages(final String filePath) throws PdfParserException {
		List<String> words = readWords(filePath);
		return makePagesWithSubtasks(words);
	}

	private List<String> readWords(final String filePath) throws PdfParserException {
		String parsedText = extractText(filePath);
		Matcher matcher = makeWordMatcher(parsedText);
		return extractWords(matcher, parsedText);
	}

	private String extractText(final String filePath) throws PdfParserException {
		PdfParser parser = new PdfParser(filePath);
		return parser.parse();
	}

	private Matcher makeWordMatcher(final String parsedText) {
		Pattern p = Pattern.compile("[\\w']+");
		return p.matcher(parsedText);
	}

	private List<String> extractWords(final Matcher matcher, final String parsedText) {
		List<String> words = new LinkedList<>();
		while (matcher.find()) {
			String word = parsedText.substring(matcher.start(), matcher.end());
			words.add(word.toLowerCase());
		}
		return words;
	}

	private List<Page> makePagesWithSubtasks(final List<String> words) {
		List<Page> pages = new LinkedList<>();
		for (int i = 0; i < words.size(); i += PAGE_SIZE) {
			waitIfPaused();
			Page page = new Page(words.subList(i, Math.min(i + PAGE_SIZE, words.size())));
			pages.add(page);

			forkCountSubtask(page);
		}
		return pages;
	}

	private void forkCountSubtask(final Page page) {
		CountWordsTask task = taskFactory.countWordsTask(page);
		task.fork();
	}*/

}
