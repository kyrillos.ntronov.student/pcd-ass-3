package pcd.ass2.wordcounter.agent.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pcd.ass2.logging.Log;
import pcd.ass2.wordcounter.service.WordCounterService;
import pcd.ass2.wordcounter.structures.data.Page;
import pcd.ass2.wordcounter.structures.data.WordCountResult;

/**
 * Parses pages and updates provided counter map
 *
 */
public class CountWordsTask extends WordCounterTask<Map<String, Integer>> {
	@Override
	protected Map<String, Integer> compute() {
		return null;
	}

	/*private static final long serialVersionUID = 5070836333150595147L;

	private static final String NAME = "PARSE_TEXT";

	private final Page page;

	private final List<String> excludedWords;

	private final Map<String, Integer> countMap;
	
	private final transient WordCounterService parentService;
	
	public CountWordsTask(final WordCounterService parentService, final Page page) {
		this.page = page;
		this.excludedWords = parentService.getExcludedWords();
		this.countMap = new HashMap<>();
		this.parentService = parentService;
	}

	@Override
	protected Map<String, Integer> compute() {
		Map<String, Integer> wordCounts = countWordsInPage();
		parentService.getWordCountBlockingQueue().add(new WordCountResult(wordCounts));
		return wordCounts;
	}
	
	private Map<String, Integer> countWordsInPage() {
		Log.info("Starting parsing page", NAME);
		for (String rawWord : page.getWords()) {
			String word = rawWord.toLowerCase();
			if (!word.isEmpty() && !excludedWords.contains(word)) {
				waitIfPaused();
				registerWordOccurence(word);
			}
		}
		parentService.getWordCountBlockingQueue().add(new WordCountResult(countMap));
		Log.info("Finished parsing page", NAME);
		return countMap;
	}

	private void registerWordOccurence(String word) {
		Integer wordOccurencies = countMap.get(word);
		if (wordOccurencies != null) {
			countMap.put(word, wordOccurencies + 1);
		} else {
			countMap.put(word, 1);
		}
	}

	@Override
	public String toString() {
		return "ParseTextWorker [name=" + NAME + "]";
	}*/

}
