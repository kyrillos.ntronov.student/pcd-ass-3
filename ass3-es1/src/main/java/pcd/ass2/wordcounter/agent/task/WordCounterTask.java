package pcd.ass2.wordcounter.agent.task;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public abstract class WordCounterTask<V> extends RecursiveTask<V> {

	private static final long serialVersionUID = -7613544339445261126L;

	private final List<WordCounterTask<?>> forks = new LinkedList<>();

	private volatile boolean isPaused;

	public void setIsPaused(boolean isPaused) {
		this.isPaused = isPaused;
	}

	protected void waitIfPaused() {
		while (isPaused)
			;
	}
	
	protected void awaitForks() {
		for(WordCounterTask<?> fork : forks) {
			fork.join();
		}
	}

	public List<WordCounterTask<?>> getForks() {
		return forks;
	}

}
